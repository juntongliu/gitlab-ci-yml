#!/usr/bin/env python

import glob
import sys
import requests
import os

GITLAB_CI_LINTER_URI = 'https://gitlab.esss.lu.se/api/v4/projects/{}/ci/lint'.format(os.getenv('CI_PROJECT_ID'))


def validate(filename):
    with open(filename) as f:
        content = f.read()
    headers = {'Authorization': 'Bearer {}'.format(os.getenv('CI_LINT_TOKEN')) }
    r = requests.post(GITLAB_CI_LINTER_URI, headers=headers, data={'content': content})
    if r.json().get('valid'):
        print(f'valid: {filename}')
    else:
        print(f'invalid: {filename}')
        print(r.json())
        sys.exit(1)


def main():
    if len(sys.argv) > 1:
        filenames = sys.argv[1:]
    else:
        filenames = glob.glob('*.gitlab-ci.yml')
    for filename in filenames:
        validate(filename)


if __name__ == '__main__':
    main()
